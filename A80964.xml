<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A80964">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>An ordinance of explanation touching treasons</title>
    <author>England and Wales. Lord Protector (1653-1658 : O. Cromwell)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A80964 of text R211787 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.17[77]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A80964</idno>
    <idno type="STC">Wing C7134A</idno>
    <idno type="STC">Thomason 669.f.17[77]</idno>
    <idno type="STC">ESTC R211787</idno>
    <idno type="EEBO-CITATION">99870486</idno>
    <idno type="PROQUEST">99870486</idno>
    <idno type="VID">163325</idno>
    <idno type="PROQUESTGOID">2248506453</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A80964)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163325)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f17[77])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>An ordinance of explanation touching treasons</title>
      <author>England and Wales. Lord Protector (1653-1658 : O. Cromwell)</author>
      <author>England and Wales. Council of State.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by William du-Gard and Henry Hills, printers to His Highness the Lord Protector,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1653 [i.e. 1654]</date>
     </publicationStmt>
     <notesStmt>
      <note>Order to print dated: Fryday, February 17. 1653. Signed: Hen. Scobell, Clerk of the Council.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Treason -- England -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1649-1660 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A80964</ep:tcp>
    <ep:estc> R211787</ep:estc>
    <ep:stc> (Thomason 669.f.17[77]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>An ordinance of explanation touching treasons.</ep:title>
    <ep:author>England and Wales. Lord Protector</ep:author>
    <ep:publicationYear>1654</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>333</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-10</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-10</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-11</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-11</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A80964-e10">
  <body xml:id="A80964-e20">
   <div type="ordinance" xml:id="A80964-e30">
    <head type="illustration" xml:id="A80964-e40">
     <figure xml:id="A80964-e50">
      <figDesc xml:id="A80964-e60">blazon or coat of arms</figDesc>
     </figure>
    </head>
    <pb facs="tcp:163325:1" rend="simple:additions" xml:id="A80964-001-a"/>
    <head xml:id="A80964-e70">
     <w lemma="a" pos="d" xml:id="A80964-001-a-0010">AN</w>
     <w lemma="ordinance" pos="n1" xml:id="A80964-001-a-0020">ORDINANCE</w>
     <w lemma="of" pos="acp" xml:id="A80964-001-a-0030">OF</w>
     <w lemma="explanation" pos="n1" xml:id="A80964-001-a-0040">EXPLANATION</w>
     <w lemma="touch" pos="vvg" xml:id="A80964-001-a-0050">TOUCHING</w>
     <w lemma="treason" pos="n2" xml:id="A80964-001-a-0060">Treasons</w>
     <pc unit="sentence" xml:id="A80964-001-a-0070">.</pc>
    </head>
    <p xml:id="A80964-e80">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A80964-001-a-0080">WHereas</w>
     <w lemma="by" pos="acp" xml:id="A80964-001-a-0090">by</w>
     <w lemma="a" pos="d" xml:id="A80964-001-a-0100">An</w>
     <w lemma="ordinance" pos="n1" xml:id="A80964-001-a-0110">Ordinance</w>
     <pc xml:id="A80964-001-a-0120">,</pc>
     <w lemma="entitle" pos="vvn" reg="entitled" xml:id="A80964-001-a-0130">Entituled</w>
     <pc xml:id="A80964-001-a-0140">,</pc>
     <hi xml:id="A80964-e90">
      <w lemma="a" pos="d" xml:id="A80964-001-a-0150">An</w>
      <w lemma="ordinance" pos="n1" xml:id="A80964-001-a-0160">Ordinance</w>
      <w lemma="declare" pos="vvg" xml:id="A80964-001-a-0170">Declaring</w>
      <w lemma="that" pos="cs" xml:id="A80964-001-a-0180">that</w>
      <w lemma="the" pos="d" xml:id="A80964-001-a-0190">the</w>
      <w lemma="offence" pos="n2" xml:id="A80964-001-a-0200">Offences</w>
      <w lemma="therein" pos="av" xml:id="A80964-001-a-0210">therein</w>
      <w lemma="mention" pos="vvn" xml:id="A80964-001-a-0220">mentioned</w>
      <pc xml:id="A80964-001-a-0230">,</pc>
      <w lemma="and" pos="cc" xml:id="A80964-001-a-0240">and</w>
      <w lemma="no" pos="dx" xml:id="A80964-001-a-0250">no</w>
      <w lemma="other" pos="pi-d" xml:id="A80964-001-a-0260">other</w>
      <pc xml:id="A80964-001-a-0270">,</pc>
      <w lemma="shall" pos="vmb" xml:id="A80964-001-a-0280">shall</w>
      <w lemma="be" pos="vvi" xml:id="A80964-001-a-0290">be</w>
      <w lemma="adjudge" pos="vvn" xml:id="A80964-001-a-0300">adjudged</w>
      <w lemma="high" pos="j" xml:id="A80964-001-a-0310">High</w>
      <w lemma="treason" pos="n1" xml:id="A80964-001-a-0320">Treason</w>
      <w lemma="within" pos="acp" xml:id="A80964-001-a-0330">within</w>
      <w lemma="the" pos="d" xml:id="A80964-001-a-0340">the</w>
      <w lemma="commonwealth" pos="n1" reg="commonwealth" xml:id="A80964-001-a-0350">Common-wealth</w>
      <w lemma="of" pos="acp" xml:id="A80964-001-a-0360">of</w>
      <hi xml:id="A80964-e100">
       <w lemma="England" pos="nn1" xml:id="A80964-001-a-0370">England</w>
       <pc xml:id="A80964-001-a-0380">,</pc>
       <w lemma="scotland" pos="nn1" xml:id="A80964-001-a-0390">Scotland</w>
       <pc xml:id="A80964-001-a-0400">,</pc>
      </hi>
      <w lemma="and" pos="cc" xml:id="A80964-001-a-0410">and</w>
      <hi xml:id="A80964-e110">
       <w lemma="Ireland" pos="nn1" xml:id="A80964-001-a-0420">Ireland</w>
       <pc xml:id="A80964-001-a-0430">,</pc>
      </hi>
     </hi>
     <w lemma="it" pos="pn" xml:id="A80964-001-a-0440">it</w>
     <w lemma="be" pos="vvz" xml:id="A80964-001-a-0450">is</w>
     <pc xml:id="A80964-001-a-0460">,</pc>
     <w lemma="among" pos="acp" xml:id="A80964-001-a-0470">amongst</w>
     <w lemma="other" pos="d" xml:id="A80964-001-a-0480">other</w>
     <w lemma="thing" pos="n2" xml:id="A80964-001-a-0490">things</w>
     <pc xml:id="A80964-001-a-0500">,</pc>
     <w lemma="ordain" pos="vvn" xml:id="A80964-001-a-0510">Ordained</w>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-0520">and</w>
     <w lemma="establish" pos="vvn" xml:id="A80964-001-a-0530">Established</w>
     <pc xml:id="A80964-001-a-0540">,</pc>
     <hi xml:id="A80964-e120">
      <w lemma="that" pos="cs" xml:id="A80964-001-a-0550">That</w>
      <w lemma="no" pos="dx" xml:id="A80964-001-a-0560">no</w>
      <w lemma="matter" pos="n1" xml:id="A80964-001-a-0570">Matter</w>
      <pc xml:id="A80964-001-a-0580">,</pc>
      <w lemma="fact" pos="n1" xml:id="A80964-001-a-0590">Fact</w>
      <pc xml:id="A80964-001-a-0600">,</pc>
      <w lemma="crime" pos="n1" xml:id="A80964-001-a-0610">Crime</w>
      <w lemma="or" pos="cc" xml:id="A80964-001-a-0620">or</w>
      <w lemma="offence" pos="n1" xml:id="A80964-001-a-0630">Offence</w>
      <w lemma="whatsoever" pos="crq" xml:id="A80964-001-a-0640">whatsoever</w>
      <pc xml:id="A80964-001-a-0650">,</pc>
      <w lemma="other" pos="pi-d" xml:id="A80964-001-a-0660">other</w>
      <w lemma="than" pos="cs" xml:id="A80964-001-a-0670">than</w>
      <w lemma="such" pos="d" xml:id="A80964-001-a-0680">such</w>
      <w lemma="as" pos="acp" xml:id="A80964-001-a-0690">as</w>
      <w lemma="be" pos="vvb" xml:id="A80964-001-a-0700">are</w>
      <w lemma="therein" pos="av" xml:id="A80964-001-a-0710">therein</w>
      <w lemma="mention" pos="vvn" xml:id="A80964-001-a-0720">mentioned</w>
      <w lemma="and" pos="cc" xml:id="A80964-001-a-0730">and</w>
      <w lemma="express" pos="vvn" xml:id="A80964-001-a-0740">expressed</w>
      <pc xml:id="A80964-001-a-0750">,</pc>
      <w lemma="shall" pos="vmd" xml:id="A80964-001-a-0760">should</w>
      <w lemma="be" pos="vvi" xml:id="A80964-001-a-0770">be</w>
      <w lemma="deem" pos="vvn" xml:id="A80964-001-a-0780">deemed</w>
      <pc xml:id="A80964-001-a-0790">,</pc>
      <w lemma="take" pos="vvn" xml:id="A80964-001-a-0800">taken</w>
      <pc xml:id="A80964-001-a-0810">,</pc>
      <w lemma="or" pos="cc" xml:id="A80964-001-a-0820">or</w>
      <w lemma="adjudge" pos="vvn" xml:id="A80964-001-a-0830">adjudged</w>
      <w lemma="to" pos="prt" xml:id="A80964-001-a-0840">to</w>
      <w lemma="be" pos="vvi" xml:id="A80964-001-a-0850">be</w>
      <w lemma="high" pos="j" xml:id="A80964-001-a-0860">High</w>
      <w lemma="treason" pos="n1" xml:id="A80964-001-a-0870">Treason</w>
      <pc xml:id="A80964-001-a-0880">;</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-0890">And</w>
     <w lemma="whereas" pos="cs" xml:id="A80964-001-a-0900">whereas</w>
     <w lemma="some" pos="d" xml:id="A80964-001-a-0910">some</w>
     <w lemma="doubt" pos="n2" xml:id="A80964-001-a-0920">doubts</w>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-0930">and</w>
     <w lemma="question" pos="n1" xml:id="A80964-001-a-0940">question</w>
     <w lemma="may" pos="vmb" xml:id="A80964-001-a-0950">may</w>
     <w lemma="arise" pos="vvi" xml:id="A80964-001-a-0960">arise</w>
     <pc xml:id="A80964-001-a-0970">,</pc>
     <w lemma="whether" pos="cs" xml:id="A80964-001-a-0980">whether</w>
     <w lemma="that" pos="d" xml:id="A80964-001-a-0990">that</w>
     <w lemma="clause" pos="n1" xml:id="A80964-001-a-1000">Clause</w>
     <w lemma="in" pos="acp" xml:id="A80964-001-a-1010">in</w>
     <w lemma="the" pos="d" xml:id="A80964-001-a-1020">the</w>
     <w lemma="say" pos="j-vn" xml:id="A80964-001-a-1030">said</w>
     <w lemma="ordinance" pos="n1" xml:id="A80964-001-a-1040">Ordinance</w>
     <w lemma="do" pos="vvz" xml:id="A80964-001-a-1050">doth</w>
     <w lemma="not" pos="xx" xml:id="A80964-001-a-1060">not</w>
     <w lemma="extend" pos="vvi" xml:id="A80964-001-a-1070">extend</w>
     <w lemma="to" pos="acp" xml:id="A80964-001-a-1080">to</w>
     <w lemma="the" pos="d" xml:id="A80964-001-a-1090">the</w>
     <w lemma="offence" pos="n2" xml:id="A80964-001-a-1100">Offences</w>
     <w lemma="mention" pos="vvn" xml:id="A80964-001-a-1110">mentioned</w>
     <w lemma="in" pos="acp" xml:id="A80964-001-a-1120">in</w>
     <w lemma="the" pos="d" xml:id="A80964-001-a-1130">the</w>
     <w lemma="instrument" pos="n1" xml:id="A80964-001-a-1140">Instrument</w>
     <pc xml:id="A80964-001-a-1150">,</pc>
     <w lemma="entitle" pos="vvn" reg="entitled" xml:id="A80964-001-a-1160">Entituled</w>
     <pc xml:id="A80964-001-a-1170">,</pc>
     <hi xml:id="A80964-e130">
      <w lemma="the" pos="d" xml:id="A80964-001-a-1180">The</w>
      <w lemma="government" pos="n1" xml:id="A80964-001-a-1190">Government</w>
      <w lemma="of" pos="acp" xml:id="A80964-001-a-1200">of</w>
      <w lemma="the" pos="d" xml:id="A80964-001-a-1210">the</w>
      <w lemma="commonwealth" pos="n1" reg="commonwealth" xml:id="A80964-001-a-1220">Common-wealth</w>
      <w lemma="of" pos="acp" xml:id="A80964-001-a-1230">of</w>
      <hi xml:id="A80964-e140">
       <w lemma="England" pos="nn1" xml:id="A80964-001-a-1240">England</w>
       <pc xml:id="A80964-001-a-1250">,</pc>
       <w lemma="scotland" pos="nn1" xml:id="A80964-001-a-1260">Scotland</w>
       <pc xml:id="A80964-001-a-1270">,</pc>
      </hi>
      <w lemma="and" pos="cc" xml:id="A80964-001-a-1280">and</w>
      <hi xml:id="A80964-e150">
       <w lemma="Ireland" pos="nn1" xml:id="A80964-001-a-1290">Ireland</w>
       <pc xml:id="A80964-001-a-1300">,</pc>
      </hi>
      <w lemma="and" pos="cc" xml:id="A80964-001-a-1310">and</w>
      <w lemma="the" pos="d" xml:id="A80964-001-a-1320">the</w>
      <w lemma="dominion" pos="n2" xml:id="A80964-001-a-1330">Dominions</w>
      <w lemma="thereto" pos="av" xml:id="A80964-001-a-1340">thereto</w>
      <w lemma="belong" pos="vvg" xml:id="A80964-001-a-1350">belonging</w>
      <pc xml:id="A80964-001-a-1360">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-1370">and</w>
     <w lemma="which" pos="crq" xml:id="A80964-001-a-1380">which</w>
     <w lemma="be" pos="vvb" xml:id="A80964-001-a-1390">are</w>
     <w lemma="thereby" pos="av" xml:id="A80964-001-a-1400">thereby</w>
     <w lemma="declare" pos="vvn" xml:id="A80964-001-a-1410">Declared</w>
     <w lemma="to" pos="prt" xml:id="A80964-001-a-1420">to</w>
     <w lemma="be" pos="vvi" xml:id="A80964-001-a-1430">be</w>
     <w lemma="high" pos="j" xml:id="A80964-001-a-1440">High</w>
     <w lemma="treason" pos="n1" xml:id="A80964-001-a-1450">Treason</w>
     <pc xml:id="A80964-001-a-1460">;</pc>
     <w lemma="for" pos="acp" xml:id="A80964-001-a-1470">For</w>
     <w lemma="clear" pos="vvg" xml:id="A80964-001-a-1480">clearing</w>
     <w lemma="thereof" pos="av" xml:id="A80964-001-a-1490">thereof</w>
     <pc xml:id="A80964-001-a-1500">,</pc>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-1510">and</w>
     <w lemma="remove" pos="vvg" xml:id="A80964-001-a-1520">removing</w>
     <w lemma="all" pos="d" xml:id="A80964-001-a-1530">all</w>
     <w lemma="scruple" pos="n2" xml:id="A80964-001-a-1540">scruples</w>
     <w lemma="thereupon" pos="av" xml:id="A80964-001-a-1550">thereupon</w>
     <pc xml:id="A80964-001-a-1560">,</pc>
     <w lemma="his" pos="po" xml:id="A80964-001-a-1570">His</w>
     <w lemma="highness" pos="n1" xml:id="A80964-001-a-1580">Highness</w>
     <w lemma="the" pos="d" xml:id="A80964-001-a-1590">the</w>
     <w lemma="lord" pos="n1" xml:id="A80964-001-a-1600">Lord</w>
     <w lemma="protector" pos="n1" xml:id="A80964-001-a-1610">Protector</w>
     <pc xml:id="A80964-001-a-1620">,</pc>
     <w lemma="by" pos="acp" xml:id="A80964-001-a-1630">by</w>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-1640">and</w>
     <w lemma="with" pos="acp" xml:id="A80964-001-a-1650">with</w>
     <w lemma="the" pos="d" xml:id="A80964-001-a-1660">the</w>
     <w lemma="advice" pos="n1" xml:id="A80964-001-a-1670">advice</w>
     <w lemma="of" pos="acp" xml:id="A80964-001-a-1680">of</w>
     <w lemma="his" pos="po" xml:id="A80964-001-a-1690">his</w>
     <w lemma="council" pos="n1" xml:id="A80964-001-a-1700">Council</w>
     <pc xml:id="A80964-001-a-1710">,</pc>
     <w lemma="do" pos="vvz" xml:id="A80964-001-a-1720">Doth</w>
     <w lemma="declare" pos="vvi" xml:id="A80964-001-a-1730">Declare</w>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-1740">and</w>
     <w lemma="ordain" pos="vvi" xml:id="A80964-001-a-1750">Ordain</w>
     <pc xml:id="A80964-001-a-1760">,</pc>
     <w lemma="that" pos="cs" xml:id="A80964-001-a-1770">That</w>
     <w lemma="neither" pos="avx-d" xml:id="A80964-001-a-1780">neither</w>
     <w lemma="the" pos="d" xml:id="A80964-001-a-1790">the</w>
     <w lemma="say" pos="j-vn" xml:id="A80964-001-a-1800">said</w>
     <w lemma="ordinance" pos="n1" xml:id="A80964-001-a-1810">Ordinance</w>
     <pc xml:id="A80964-001-a-1820">,</pc>
     <w lemma="nor" pos="ccx" xml:id="A80964-001-a-1830">nor</w>
     <w lemma="any" pos="d" xml:id="A80964-001-a-1840">any</w>
     <w lemma="clause" pos="n1" xml:id="A80964-001-a-1850">Clause</w>
     <w lemma="or" pos="cc" xml:id="A80964-001-a-1860">or</w>
     <w lemma="thing" pos="n1" xml:id="A80964-001-a-1870">thing</w>
     <w lemma="therein" pos="av" xml:id="A80964-001-a-1880">therein</w>
     <w lemma="contain" pos="vvn" xml:id="A80964-001-a-1890">contained</w>
     <pc xml:id="A80964-001-a-1900">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A80964-001-a-1910">shall</w>
     <w lemma="extend" pos="vvi" xml:id="A80964-001-a-1920">extend</w>
     <w lemma="or" pos="cc" xml:id="A80964-001-a-1930">or</w>
     <w lemma="be" pos="vvi" xml:id="A80964-001-a-1940">be</w>
     <w lemma="construe" pos="vvn" xml:id="A80964-001-a-1950">construed</w>
     <pc xml:id="A80964-001-a-1960">,</pc>
     <w lemma="adjudge" pos="vvn" xml:id="A80964-001-a-1970">adjudged</w>
     <w lemma="or" pos="cc" xml:id="A80964-001-a-1980">or</w>
     <w lemma="take" pos="vvn" xml:id="A80964-001-a-1990">taken</w>
     <w lemma="to" pos="prt" xml:id="A80964-001-a-2000">to</w>
     <w lemma="extend" pos="vvi" xml:id="A80964-001-a-2010">extend</w>
     <w lemma="unto" pos="acp" xml:id="A80964-001-a-2020">unto</w>
     <w lemma="all" pos="d" xml:id="A80964-001-a-2030">all</w>
     <w lemma="or" pos="cc" xml:id="A80964-001-a-2040">or</w>
     <w lemma="any" pos="d" xml:id="A80964-001-a-2050">any</w>
     <w lemma="the" pos="d" xml:id="A80964-001-a-2060">the</w>
     <w lemma="offence" pos="n2" xml:id="A80964-001-a-2070">Offences</w>
     <w lemma="declare" pos="vvn" xml:id="A80964-001-a-2080">declared</w>
     <w lemma="by" pos="acp" xml:id="A80964-001-a-2090">by</w>
     <w lemma="the" pos="d" xml:id="A80964-001-a-2100">the</w>
     <w lemma="say" pos="j-vn" xml:id="A80964-001-a-2110">said</w>
     <w lemma="instrument" pos="n1" xml:id="A80964-001-a-2120">Instrument</w>
     <w lemma="to" pos="prt" xml:id="A80964-001-a-2130">to</w>
     <w lemma="be" pos="vvi" xml:id="A80964-001-a-2140">be</w>
     <w lemma="high" pos="j" xml:id="A80964-001-a-2150">High</w>
     <w lemma="treason" pos="n1" xml:id="A80964-001-a-2160">Treason</w>
     <pc xml:id="A80964-001-a-2170">,</pc>
     <w lemma="but" pos="acp" xml:id="A80964-001-a-2180">but</w>
     <w lemma="that" pos="cs" xml:id="A80964-001-a-2190">that</w>
     <w lemma="all" pos="d" xml:id="A80964-001-a-2200">all</w>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-2210">and</w>
     <w lemma="every" pos="d" xml:id="A80964-001-a-2220">every</w>
     <w lemma="the" pos="d" xml:id="A80964-001-a-2230">the</w>
     <w lemma="say" pos="j-vn" xml:id="A80964-001-a-2240">said</w>
     <w lemma="offence" pos="n2" xml:id="A80964-001-a-2250">Offences</w>
     <w lemma="be" pos="vvb" xml:id="A80964-001-a-2260">are</w>
     <w lemma="hereby" pos="av" xml:id="A80964-001-a-2270">hereby</w>
     <w lemma="adjudge" pos="vvn" xml:id="A80964-001-a-2280">adjudged</w>
     <pc xml:id="A80964-001-a-2290">,</pc>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-2300">and</w>
     <w lemma="shall" pos="vmb" xml:id="A80964-001-a-2310">shall</w>
     <w lemma="be" pos="vvi" xml:id="A80964-001-a-2320">be</w>
     <w lemma="adjudge" pos="vvn" xml:id="A80964-001-a-2330">adjudged</w>
     <pc xml:id="A80964-001-a-2340">,</pc>
     <w lemma="take" pos="vvn" xml:id="A80964-001-a-2350">taken</w>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-2360">and</w>
     <w lemma="deem" pos="vvn" xml:id="A80964-001-a-2370">deemed</w>
     <w lemma="to" pos="prt" xml:id="A80964-001-a-2380">to</w>
     <w lemma="be" pos="vvi" xml:id="A80964-001-a-2390">be</w>
     <w lemma="high" pos="j" xml:id="A80964-001-a-2400">High</w>
     <w lemma="treason" pos="n1" xml:id="A80964-001-a-2410">Treason</w>
     <pc xml:id="A80964-001-a-2420">,</pc>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-2430">and</w>
     <w lemma="that" pos="cs" xml:id="A80964-001-a-2440">that</w>
     <w lemma="all" pos="d" xml:id="A80964-001-a-2450">all</w>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-2460">and</w>
     <w lemma="every" pos="d" xml:id="A80964-001-a-2470">every</w>
     <w lemma="the" pos="d" xml:id="A80964-001-a-2480">the</w>
     <w lemma="offender" pos="n1" reg="offender" xml:id="A80964-001-a-2490">Offendor</w>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-2500">and</w>
     <w lemma="offender" pos="n2" reg="offenders" xml:id="A80964-001-a-2510">Offendors</w>
     <w lemma="shall" pos="vmb" xml:id="A80964-001-a-2520">shall</w>
     <w lemma="suffer" pos="vvi" xml:id="A80964-001-a-2530">suffer</w>
     <w lemma="the" pos="d" xml:id="A80964-001-a-2540">the</w>
     <w lemma="pain" pos="n2" xml:id="A80964-001-a-2550">pains</w>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-2560">and</w>
     <w lemma="penalty" pos="n2" xml:id="A80964-001-a-2570">penalties</w>
     <w lemma="of" pos="acp" xml:id="A80964-001-a-2580">of</w>
     <w lemma="high" pos="j" xml:id="A80964-001-a-2590">High</w>
     <w lemma="treason" pos="n1" xml:id="A80964-001-a-2600">Treason</w>
     <pc xml:id="A80964-001-a-2610">,</pc>
     <w lemma="the" pos="d" xml:id="A80964-001-a-2620">the</w>
     <w lemma="say" pos="j-vn" xml:id="A80964-001-a-2630">said</w>
     <w lemma="ordinance" pos="n1" xml:id="A80964-001-a-2640">Ordinance</w>
     <pc xml:id="A80964-001-a-2650">,</pc>
     <w lemma="or" pos="cc" xml:id="A80964-001-a-2660">or</w>
     <w lemma="any" pos="d" xml:id="A80964-001-a-2670">any</w>
     <w lemma="thing" pos="n1" xml:id="A80964-001-a-2680">thing</w>
     <w lemma="therein" pos="av" xml:id="A80964-001-a-2690">therein</w>
     <w lemma="contain" pos="vvn" xml:id="A80964-001-a-2700">contained</w>
     <w lemma="to" pos="acp" xml:id="A80964-001-a-2710">to</w>
     <w lemma="the" pos="d" xml:id="A80964-001-a-2720">the</w>
     <w lemma="contrary" pos="j" xml:id="A80964-001-a-2730">contrary</w>
     <pc xml:id="A80964-001-a-2740">,</pc>
     <w lemma="in" pos="acp" xml:id="A80964-001-a-2750">in</w>
     <w lemma="any" pos="d" xml:id="A80964-001-a-2760">any</w>
     <w lemma="wise" pos="j" xml:id="A80964-001-a-2770">wise</w>
     <w lemma="notwithstanding" pos="acp" xml:id="A80964-001-a-2780">notwithstanding</w>
     <pc unit="sentence" xml:id="A80964-001-a-2790">.</pc>
    </p>
    <div type="license" xml:id="A80964-e160">
     <opener xml:id="A80964-e170">
      <dateline xml:id="A80964-e180">
       <date xml:id="A80964-e190">
        <w lemma="Friday" pos="nn1" reg="Friday" xml:id="A80964-001-a-2800">Fryday</w>
        <pc xml:id="A80964-001-a-2810">,</pc>
        <w lemma="February" pos="nn1" xml:id="A80964-001-a-2820">February</w>
        <w lemma="17." pos="crd" xml:id="A80964-001-a-2830">17.</w>
        <w lemma="1653." pos="crd" xml:id="A80964-001-a-2840">1653.</w>
        <pc unit="sentence" xml:id="A80964-001-a-2850"/>
       </date>
      </dateline>
     </opener>
     <p xml:id="A80964-e200">
      <w lemma="order" pos="j-vn" xml:id="A80964-001-a-2860">ORdered</w>
      <w lemma="by" pos="acp" xml:id="A80964-001-a-2870">by</w>
      <w lemma="his" pos="po" xml:id="A80964-001-a-2880">his</w>
      <w lemma="highness" pos="n1" xml:id="A80964-001-a-2890">Highness</w>
      <w lemma="the" pos="d" xml:id="A80964-001-a-2900">the</w>
      <w lemma="lord" pos="n1" xml:id="A80964-001-a-2910">Lord</w>
      <w lemma="protector" pos="n1" xml:id="A80964-001-a-2920">Protector</w>
      <pc xml:id="A80964-001-a-2930">,</pc>
      <w lemma="and" pos="cc" xml:id="A80964-001-a-2940">and</w>
      <w lemma="his" pos="po" xml:id="A80964-001-a-2950">his</w>
      <w lemma="council" pos="n1" xml:id="A80964-001-a-2960">Council</w>
      <pc xml:id="A80964-001-a-2970">,</pc>
      <w lemma="that" pos="cs" xml:id="A80964-001-a-2980">That</w>
      <w lemma="this" pos="d" xml:id="A80964-001-a-2990">this</w>
      <w lemma="ordinance" pos="n1" xml:id="A80964-001-a-3000">Ordinance</w>
      <w lemma="be" pos="vvb" xml:id="A80964-001-a-3010">be</w>
      <w lemma="forthwith" pos="av" xml:id="A80964-001-a-3020">forthwith</w>
      <w lemma="print" pos="vvn" xml:id="A80964-001-a-3030">Printed</w>
      <w lemma="and" pos="cc" xml:id="A80964-001-a-3040">and</w>
      <w lemma="publish" pos="vvn" xml:id="A80964-001-a-3050">Published</w>
      <pc unit="sentence" xml:id="A80964-001-a-3060">.</pc>
     </p>
     <closer xml:id="A80964-e210">
      <signed xml:id="A80964-e220">
       <w lemma="HEN." pos="ab" xml:id="A80964-001-a-3070">HEN.</w>
       <w lemma="Scobell" pos="nn1" xml:id="A80964-001-a-3090">SCOBELL</w>
       <pc xml:id="A80964-001-a-3100">,</pc>
       <w lemma="clerk" pos="n1" xml:id="A80964-001-a-3110">Clerk</w>
       <w lemma="of" pos="acp" xml:id="A80964-001-a-3120">of</w>
       <w lemma="the" pos="d" xml:id="A80964-001-a-3130">the</w>
       <w lemma="council" pos="n1" xml:id="A80964-001-a-3140">Council</w>
      </signed>
     </closer>
    </div>
   </div>
  </body>
  <back xml:id="A80964-e230">
   <div type="colophon" xml:id="A80964-e240">
    <p xml:id="A80964-e250">
     <hi xml:id="A80964-e260">
      <w lemma="London" pos="nn1" xml:id="A80964-001-a-3150">London</w>
      <pc xml:id="A80964-001-a-3160">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A80964-001-a-3170">Printed</w>
     <w lemma="by" pos="acp" xml:id="A80964-001-a-3180">by</w>
     <hi xml:id="A80964-e270">
      <w lemma="William" pos="nn1" xml:id="A80964-001-a-3190">William</w>
      <w lemma="du-gard" pos="nn1" xml:id="A80964-001-a-3200">du-Gard</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A80964-001-a-3210">and</w>
     <hi xml:id="A80964-e280">
      <w lemma="Henry" pos="nn1" xml:id="A80964-001-a-3220">Henry</w>
      <w lemma="hill" pos="n2" xml:id="A80964-001-a-3230">Hills</w>
      <pc xml:id="A80964-001-a-3240">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A80964-001-a-3250">Printers</w>
     <w lemma="to" pos="acp" xml:id="A80964-001-a-3260">to</w>
     <w lemma="his" pos="po" xml:id="A80964-001-a-3270">his</w>
     <w lemma="highness" pos="n1" xml:id="A80964-001-a-3280">Highness</w>
     <w lemma="the" pos="d" xml:id="A80964-001-a-3290">the</w>
     <w lemma="lord" pos="n1" xml:id="A80964-001-a-3300">Lord</w>
     <w lemma="protector" pos="n1" xml:id="A80964-001-a-3310">Protector</w>
     <pc xml:id="A80964-001-a-3320">,</pc>
     <w lemma="1653." pos="crd" xml:id="A80964-001-a-3330">1653.</w>
     <pc unit="sentence" xml:id="A80964-001-a-3340"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
